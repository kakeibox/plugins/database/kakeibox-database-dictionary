from kakeibox_database_dictionary.engine import TableTransactionCategory


class DeleteTransactionCategoryRepositoryPort():

    def execute(self, transaction_category_code):
        try:
            categories = TableTransactionCategory()
            del categories.table[transaction_category_code]
            return True
        except Exception:
            return False

