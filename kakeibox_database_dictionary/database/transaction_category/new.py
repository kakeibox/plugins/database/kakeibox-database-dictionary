from kakeibox_database_dictionary.engine import TableTransactionCategory


class NewTransactionCategoryRepositoryPort():

    def execute(self, new_data_dictionary):
        code = new_data_dictionary['code']
        categories = TableTransactionCategory()
        categories.table[code] = new_data_dictionary
        return categories.table[code]
