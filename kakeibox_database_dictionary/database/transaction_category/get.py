from kakeibox_database_dictionary.engine import TableTransactionCategory


class GetByCodeTransactionCategoryRepositoryPort():

    def execute(self, transaction_category_code):
        categories = TableTransactionCategory()
        return categories.table[transaction_category_code]
