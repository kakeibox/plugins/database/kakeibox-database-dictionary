from kakeibox_database_dictionary.engine import TableTransactionCategory


class UpdateTransactionCategoryRepositoryPort():

    def execute(self, code, update_dict_data):
        categories = TableTransactionCategory()
        table = categories.table
        if code in table:
            table[code] = update_dict_data
        return table[code]
