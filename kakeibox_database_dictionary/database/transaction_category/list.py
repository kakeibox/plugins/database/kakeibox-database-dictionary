from kakeibox_database_dictionary.engine import TableTransactionCategory


class ListTransactionCategoryRepositoryPort():

    def execute(self):
        categories = TableTransactionCategory()
        return [item for item in categories.table.values()]
